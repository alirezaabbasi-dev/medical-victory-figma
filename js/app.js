let $ = document;
const mobileMenuNav = $.querySelector(".header__nav-wrapper");
const mobileMenu = $.querySelector(".menu-mobile");

mobileMenuNav.addEventListener("click", function () {
  mobileMenu.classList.toggle("menu-mobile--open");
});
